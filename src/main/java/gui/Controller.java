package gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import service.LoopService;
import starting.Main;

import java.io.File;

public class Controller {

    @FXML
    Button btn_Switch;

    @FXML
    TextField txtTime;

    @FXML
    Button btn_chooseFile;

    @FXML
    Label lbl_path;

    LoopService loopService;

    private File file;

    public static String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }



    public static void playSound(String source) {
        Media media = new Media(new File(source).toURI().toString());
        final MediaPlayer player = new MediaPlayer(media);
        player.setAutoPlay(true);
    }

    public void playSound(ActionEvent actionEvent) {
        playSound(file.toString());
    }

    public void toggleButton(ActionEvent actionEvent) {

        int timeInSeconds = Integer.parseInt(txtTime.getText());

        if (loopService != null) {
            loopService.setButtonStatus(!loopService.getButtonStatus());
            loopService = null;
        } else {
            loopService = new LoopService(timeInSeconds);
            loopService.setButtonStatus(!loopService.getButtonStatus());
            loopService.start();
        }
    }


    public void chooseFile(ActionEvent actionEvent) {
        FileChooser fc = new FileChooser();
        fc.setTitle("Choose mp3");
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter(
                        "Sound Files",
                        "*.mp3"));

        File file = fc.showOpenDialog(Main.primaryStage);
        setPath(file.toString());

        if (file != null) {
            lbl_path.setText(file.toString());
            lbl_path.setTextFill(Color.BLACK);
        }
    }

}