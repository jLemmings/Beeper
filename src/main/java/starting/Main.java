package starting;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.Window;

public class Main extends Application {


    public static Window primaryStage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/Sample.fxml"));
        primaryStage.setTitle("Beeper");
        primaryStage.setScene(new Scene(root, 250, 200));

        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/Icon.png")));

        primaryStage.setResizable(false);

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
